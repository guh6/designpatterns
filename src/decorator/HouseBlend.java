package decorator;

public class HouseBlend extends Beverage {
	public HouseBlend(){
		this.description = "House Blend Coffee";
	}
	
	// House Blend's custom cost 
	public float cost() {
		return 0.50f;
	}
}
