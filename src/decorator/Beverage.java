package decorator;

public abstract class Beverage {
	// Won't ever see this description. 
	// Use Protected so that the subclasses can see description but others can't. 
	protected String description = "Unknown Beverage";
	
	// Each concrete will inherit this method
	public String getDescription(){
		return description;
	}
	
	// Abstract so each beverage will need to figure out it's own cost
	public abstract float cost();
}
