package decorator;

public abstract class CondimentDecorator extends Beverage {
	// Require the condiment to have its own description
	public abstract String getDescription();
}
