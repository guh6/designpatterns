package decorator;

public class Expresso extends Beverage{
	public Expresso(){
		this.description = "Expresso";
	}
	
	public float cost(){
		return 1.50f;
	}
}
