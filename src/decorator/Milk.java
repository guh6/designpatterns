package decorator;

public class Milk extends CondimentDecorator{
	private Beverage beverage;
	
	public Milk(Beverage beverage){
		this.beverage = beverage;
	}
	
	public String getDescription(){
		return this.beverage.getDescription() + ", MILK";
	}
	
	public float cost(){
		return this.beverage.cost() + 0.10f;
	}
}
