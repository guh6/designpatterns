package decorator;

public class CoffeeShop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Beverage beverage = new Expresso();
		System.out.println(beverage.getDescription() + " | " + beverage.cost());
		Beverage beverageWithIce = new Ice(beverage);
		System.out.println(beverageWithIce.getDescription() + " | " + beverageWithIce.cost());
		Beverage beverageWithIceAndMilk = new Milk(beverageWithIce);
		System.out.println(beverageWithIceAndMilk.getDescription() + " | " + beverageWithIceAndMilk.cost());
		
		Beverage houseBeverage = new HouseBlend();
		Beverage houseBeverageWithMilk = new Milk(houseBeverage);
		System.out.println(houseBeverageWithMilk.getDescription() + " | " + houseBeverageWithMilk.cost());
	}

}
