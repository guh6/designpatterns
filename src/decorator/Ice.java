package decorator;

public class Ice extends CondimentDecorator {
	
	Beverage beverage;
	
	public Ice(Beverage beverage){
		this.beverage = beverage;
	}
	

	@Override
	public String getDescription() {
		return this.beverage.getDescription() + ", ICE";
	}

	@Override
	public float cost() {
		return this.beverage.cost() + 11.0f;
	}

}
