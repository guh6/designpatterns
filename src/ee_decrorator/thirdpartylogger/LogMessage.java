package ee_decrorator.thirdpartylogger;

public interface LogMessage {
    void printMessage();
    String getMessage();
    void setMessage(String message);
}
