package ee_decrorator.customlogger;

import ee_decrorator.thirdpartylogger.LogMessage;

@ComplexMessage
public class SystemMessage implements LogMessage {
    @Override
    public void printMessage() {

    }

    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public void setMessage(String message) {

    }
}
