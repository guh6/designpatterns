package ee_observer;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.NotificationOptions;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ForkJoinPool;

@Stateless
@Path("/customers")
public class CustomerEndpoint {

    // This is the Subject that the Observers look for
    @Inject @CustomerEvent(CustomerEvent.Type.ADD)
    private Event<Customer> customerAddEvent;

    @Inject @CustomerEvent(CustomerEvent.Type.REMOVE)
    Event<Customer> customerRemoveEvent;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void newCustomer(Customer customer) {
        // In a scenario where there are 10 or less observers, they all get executed asynchronously
        // Returns an instance of completionStage (exception -> completionException)
        customerAddEvent.fireAsync(customer, NotificationOptions.ofExecutor(new ForkJoinPool(10)));
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeCustomer(Customer customer) {
        CompletionStage<Customer> stage = customerRemoveEvent.fireAsync(customer);
        stage.handle((Customer event, Throwable ex) -> {
            for (Throwable t : ex.getSuppressed()) { // Brings back suppressed excpetions
                // log exceptions
            }
            return event;
        });
    }
}
