pack    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void newCustomer(Customer customer) {
        // In a scenario where there are 10 or less observers, they all get executed asynchronously
        // Returns an instance of completionStage (exception -> completionException)
        customerAddEvent.fireAsync(customer, NotificationOptions.ofExecutor(new ForkJoinPool(10)));
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeCustomer(Customer customer) {
        CompletionStage<Customer> stage = customerRemoveEvent.fireAsync(customer);
        stage.handle((Customer event, Throwable ex) -> {
            for (Throwable t : ex.getSuppressed()) { // Brings back suppressed excpetions
                // log exceptions
            }
            return event;
        });
    }age ee_observer;

public class Customer {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
