package ee_observer.services;

import ee_observer.Customer;

import javax.annotation.Priority;
import javax.enterprise.event.Observes;

public class AuthenticationService {
    // The magic is the @Observes annotation where it expects to receive a customer object for this method
    // Which correlates with CustomerEndpoint.createCustomer(Customer customer)
    public void createAuthenticationCredentials(@Observes @Priority(100) Customer customer) {

    }
}
