package ee_observer.services;

import ee_observer.Customer;
import ee_observer.CustomerEvent;

import javax.annotation.Priority;
import javax.enterprise.event.Observes;

public class CustomerService {
    public void createCustomer(@Observes @Priority(10) @CustomerEvent(CustomerEvent.Type.ADD) Customer customer) {
        // Add new customer
    }

    public void removeCustomer(@Observes @CustomerEvent(CustomerEvent.Type.REMOVE) Customer customer) {
        // delete customer
    }
}
