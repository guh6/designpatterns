package singleton;

public class Singleton {
	private static Singleton currentInstance;
	
	private Singleton(){}
	
	public static Singleton getInstance(){
		if(currentInstance == null){
			currentInstance = new Singleton();
		}
		return currentInstance;
	}
	
	public String getDescription(){
		return "I am a Singleton!";
	}
}
