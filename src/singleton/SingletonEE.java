package singleton;

import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.Lock;
import javax.ejb.LockType;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

@ConcurrencyManagement(ConcurrencyManageType.BEAN)
@StartUp
@Singleton
public class SingletonEE {

    private Queue<Object> pooledObjects;

    @PostContruct
    void constructExpensiveObject(){
        // Do something expensive here like constructor
    }

    @AccessTimeout(value = 30, unit = TimeUnit.Seconds)
    @Lock(LockType.WRITE)
    public void returnObject(Object obj) {
        pooledObjects.offer(obj);
    }

    @Lock(LockType.READ)
    public Object borrowObject() {
        return pooledObjects.poll();
    }

}
