package factory_method;

public abstract class PizzaStore {
	String type;
	
	abstract Pizza createPizza(String item);
	
	public void orderPizza(String type){
		Pizza p = createPizza(type);
		System.out.println("===Making your pizza===");
		p.prepare();
		p.bake();
		p.cut();
	}
	
}
