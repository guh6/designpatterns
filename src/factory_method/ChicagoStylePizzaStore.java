package factory_method;

public class ChicagoStylePizzaStore extends PizzaStore {

	@Override
	Pizza createPizza(String item) {
		Pizza p = null;
		if(item.equals("c"))
		{
			p = new ChicagoCheesePizza();
		}
		return p;
	}

}
