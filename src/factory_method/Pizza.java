package factory_method;

public abstract class Pizza {
	String type;
	public Pizza(String type){
		this.type = type;
	}	
	public void prepare(){
		System.out.println("Preparing pizza " + this.type);
	};
	public void bake(){
		System.out.println("Baking pizza " + this.type);
	};
	public void cut(){
		System.out.println("Cutting pizza " + this.type);
	};
}
