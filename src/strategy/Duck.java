package strategy;

public abstract class Duck {
	// * The two variables below are actually interface objects. E.g., composition
	FlyBehavior flyBehavior;    
	QuackBehavior quackBehavior;
	
	public Duck(){
		
	};
	
	public void setFlyBheavior(FlyBehavior flyBehavior){
		this.flyBehavior = flyBehavior;
	}
	
	public void setQuackBehavior(QuackBehavior quackBehavior){
		this.quackBehavior = quackBehavior;
	}
	
	public void swim(){
		System.out.println("All ducks can swim, even decoy!");
	}
	
    // * Leave it abstract so each class can implement this method and displays itself appropriately
	public abstract void display();
	
	// * We are invoking the flyBehavior sub-class that implements the FlyBehavior (HAS-A) interface object.
	public void performQuack(){
		this.quackBehavior.quack();
	}
	
	// * Same with above
	public void performFly(){
		this.flyBehavior.fly();
	}
}
