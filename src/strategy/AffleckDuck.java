package strategy;

public class AffleckDuck extends Duck {
	
	public AffleckDuck(){
		this.flyBehavior = new FlyWithWings();
		this.quackBehavior = new Affleck();
	}
	
	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("Hello, do you need insurance?");
	}
	
}
