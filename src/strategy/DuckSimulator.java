package strategy;

public class DuckSimulator {

	public static void main(String[] args) {
		MallardDuck mallard = new MallardDuck();
		AffleckDuck affleck = new AffleckDuck();
		RubberDuck rubber = new RubberDuck();

		mallard.performFly();
		mallard.performQuack();
		
        affleck.performFly();
        affleck.performQuack();
        
        rubber.performFly();
        rubber.performQuack();
        
        rubber.setFlyBheavior(new FlyWithWings());
        rubber.performFly();

	}

}
