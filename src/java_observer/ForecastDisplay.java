package java_observer;
import java.util.Observable;
import java.util.Observer;

public class ForecastDisplay implements Observer, DisplayElement {
	
	private float currentPressure;
	private float lastPressure;
	
	// Same as before except rather than ISubject, it is Observable class.
	public ForecastDisplay(Observable observable)
	{
		observable.addObserver(this);
	}
	
	@Override
	public void update(Observable observable, Object arg) {
		// Check to make sure that the Observable object is an actual WeatherData object.
		// Rather than having the weatherData push specific data, we can pull the weatherData
		// * object via the getters to get what the observer wants. 
		if (observable instanceof WeatherData){
			WeatherData weatherData = (WeatherData) observable;
			lastPressure = currentPressure;
			currentPressure = weatherData.getPressure();
			display();
		}
	}

	@Override
	public void display() {
		System.out.println("> Forecaster - before: " + this.lastPressure + " -> now: " + this.currentPressure);
		
	}

}
