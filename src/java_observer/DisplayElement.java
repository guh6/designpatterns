package java_observer;

public interface DisplayElement {
	public void display();
}
