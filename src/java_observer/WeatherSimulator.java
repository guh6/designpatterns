package java_observer;

public class WeatherSimulator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WeatherData wd = new WeatherData();
		ForecastDisplay fd = new ForecastDisplay(wd);
		
		wd.setMeasurements(100, 100, 100);
		wd.setMeasurements(100, 100, 50);
		wd.setMeasurements(100, 100, 10);
	}

}
