package java_observer;
import java.util.Observable;

public class WeatherData extends Observable{
	private float temperature;
	private float humidity;
	private float pressure;
	
	public WeatherData(){
		
	}
	
	// The new two-step notification process:
	// 1. setChanged() to indicate a change has occurred
	// 2. notifyObservers to indicate a pull/push request is required. 
	public void measurementsChanged(){
		setChanged();
		notifyObservers();
	}
	
	
	public void setMeasurements(float temp, float humd, float press)
	{
		this.temperature = temp;
		this.humidity = humd;
		this.pressure = press;
		
		measurementsChanged();
	}
	
	public float getPressure(){
		return this.pressure;
	}
	
}
