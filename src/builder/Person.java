package builder;
/*
 * Builder pattern encapsulates object constructor and limits
 * the number of parameters that are used in the constructor. 
 * https://www.javaworld.com/article/2074938/core-java/too-many-parameters-in-java-methods-part-3-builder-pattern.html
 * 
 * Key ideas: 
 * 1. Make the Class constructor private. 
 * 2. Create a Builder static class that will create the object and use the private constructor.
 *    The builder class uses its own parameters that is fed into the private constructor. 
 * 3. Set any default parameters and use the Builder to set the other members
 * 4. Finally, return this(self) to allow chaining builder actions. 
 */
public class Person {	
	private final String name;
	private final char gender;
	private final String phoneNumber;
    private final String email;	
    /*
     * Private Constructor, notice it's only 4 parameters but imagine it being 10+
     */
	private Person(String name, char gender, String phoneNumber, String email){
		this.name = name;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}	
	@Override
	public String toString(){
		StringBuilder s = new StringBuilder("");
		s.append("Name: " + this.name)
		 .append("\nPhone Number: " + this.phoneNumber)
		 .append("\nGender: " + this.gender);
		return s.toString();
	}    	
	/*
	 * The Builder static class that will build the Person object
	 */
	public static class PersonBuilder
	{
		private String firstName;
		private String middleName;
		private String lastName;
		private char gender;
		private String phoneNumber;
		private String areaCode;
		private String email;
		
		// Default settings - Can also use static within static
		private final static String US_CODE = "+1";
		private final static char NO_GENDER = '?';
		private final static String NO_EMAIL = "";
		private final static String NO_MIDDLE = "";		
		
		// Set the defaults 	 
		{
			this.middleName = NO_MIDDLE;
			this.areaCode = US_CODE;
			this.gender = NO_GENDER;
			this.email = NO_EMAIL;
		}
		
		/*
		 * Setters - The return type must be the PersonBuilder
		 * itself so that we can chain the other setter methods
		 */
		public PersonBuilder setFirstName(String fName){
			this.firstName = fName ;
			return this;
		}
		public PersonBuilder setMiddleName(String middleName) {
			this.middleName = middleName;
			return this;
		}
		public PersonBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		public PersonBuilder setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}
		public PersonBuilder setAreaCode(String areaCode) {
			this.areaCode = areaCode;
			return this;
		}
		public PersonBuilder setGender(char g){
			this.gender = g;
			return this;
		}		
		/*
		 * Most important method: Build
		 * It builds the object at the end, so it must return the Class type
		 */
		public Person build(){
			String fullName = this.firstName + " " + this.middleName + " " + this.lastName;
			String modifiedPhoneNumber = "(" + this.areaCode + ") " + this.phoneNumber;			
			return new Person(fullName, this.gender, modifiedPhoneNumber, this.email);
		}		
	}	
}
