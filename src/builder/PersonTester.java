package builder;

import static java.lang.System.out;

public class PersonTester {
	public static void main(String args[]){
		out.println("Creating a Person");
		Person p1 = new Person.PersonBuilder().setFirstName("Bob")
				      .setGender('M')
				      .setLastName("Smith")
				      .setPhoneNumber("444-444-4444")
				      .build();		
		out.println(p1);
		Person p2 = new Person.PersonBuilder().setFirstName("Bob")
			      .setLastName("Smith")
			      .setPhoneNumber("444-444-4444")
			      .build();
		out.println(p2);

	}
}
