package observer;

public class ForecastDisplay implements Observer, DisplayElement{
	
	private Subject weatherData; 
	private float temperature;
	private float humidity;

	public ForecastDisplay(Subject weatherData){
		this.weatherData = weatherData; 
		this.weatherData.registerObserver(this);
	}
	
	@Override
	// We only use temperature and humidity from the weatherData(Publisher)
	public void update(float temp, float humidity, float pressure) {
		// TODO Auto-generated method stub
		this.temperature = temp;
		this.humidity = humidity; 
		
		display();
	}


	@Override
	public void display() {
		StringBuilder s = new StringBuilder("");
		s.append("> Forecast for now: ");
		s.append("temperature: " + this.temperature);
		
		if(this.temperature < 20){
			s.append("(too cold, brr)");
		}
		else if (this.temperature > 100)
		{
			s.append("(too hot!, help!)");
		}
		else {
			s.append("(comfortable)");
		}
		
		s.append(", humidity: " + this.humidity);
		if(this.humidity < 50){
			s.append("(need more moisture!)");
		}else
		{
			s.append("(feels like florida)");
		}
		
		System.out.println(s.toString());
	}

}
