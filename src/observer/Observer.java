package observer;

public interface Observer {
	// Mandatory method to deal with an update of a Publisher/Subject
	public void update(float temp, float humidity, float pressure);
}
