package observer;

import java.util.ArrayList;

public class WeatherData implements Subject {
	
	private ArrayList<Observer> observers;
	private float temperature;
	private float humidity;
	private float pressure;
	
	public WeatherData(){
		this.observers = new ArrayList<Observer>();
	}
	
	public float getTemperature(){
		return this.temperature;
	}
	
	public float getHumidity(){
		return this.humidity;
	}
	
	public float getPressure(){
		return this.pressure;
	}
	
	@Override
	public void registerObserver(Observer obs) {
		this.observers.add(obs);
	}

	@Override
	public void removeObserver(Observer obs) {
		this.observers.remove(obs);
	}

	@Override
	public void notifyObservers() {
		for(Observer obs : this.observers){
			obs.update(this.temperature, this.humidity, this.pressure);
		}
	}
	
	// Mimics Real-World example of when the measure is changed.
	public void setMeasurements(float temperature, float humidity, float pressure)
	{
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
	
	// Wrapper around the notifyObservers method. In case, we may need to take additional action when feature comes in. 
	public void measurementsChanged(){
		notifyObservers();
	}
	
}
