package observer;

public class WeatherDisplaysSimulator {
	public static void main(String args[]){
		WeatherData weatherData = new WeatherData();
		
		StatisticsDisplay sd = new StatisticsDisplay(weatherData);
		ForecastDisplay fd = new ForecastDisplay(weatherData);
		
		weatherData.setMeasurements(101, 10, 10);
		weatherData.setMeasurements(50, 60, 10);
		weatherData.setMeasurements(20, 100, 1);
		
	}

}
