package observer;

public interface Subject {
	// Mandatory methods when creating a Publisher/Subject.
	// -> Contains register, remove and notify observer(s). 
	public void registerObserver(Observer obs);
	public void removeObserver(Observer obs);
	public void notifyObservers();
}
