package observer;

public class StatisticsDisplay implements Observer, DisplayElement{

	private float temperature;
	private float humiditiy;
	private float pressure;
	
	// Create a ISubject that keeps track of the Publisher
	private Subject weatherData;
	
	// Do setup for the class and ALSO 
	// -> Register myself to the Publisher so that the Publisher can use my update method via IObserver.
	// -> Using ISubject allows us to substitute any other Publisher that implements ISubject 
	public StatisticsDisplay(Subject weatherData){
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}
	
	// Remove self from the Publisher dependency
	public void removeFromSubject(){
		this.weatherData.removeObserver(this);
	}

	@Override
	public void update(float temp, float humidity, float pressure) {
		// TODO Auto-generated method stub
		this.temperature = temp;
		this.humiditiy = humidity;
		this.pressure = pressure;
		
		display();
	}


	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("> Statistics Display: "  + this.temperature + ", " + this.humiditiy + ", " + this.pressure + " <");
	}

}
