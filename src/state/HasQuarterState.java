package state;

/*
 * HasQuarterState - only ejectQuarter and turnCrank states have valid
 * transactions.
 */
public class HasQuarterState implements State {
	GumballMachine machine;
	
	public HasQuarterState(GumballMachine machine) {
		this.machine = machine;
	}

	public void insertQuarter() {
		System.out.println("You cant insert another quarter!");		
	}

	public void ejectQuarter() {
		System.out.println("Here's your quarter back");
		machine.setState(machine.getNoQuarterState());
	}

	public void turnCrank() {
		System.out.println("You turned...");
		machine.setState(machine.getSoldState());
	}

	public void dispense() {
		System.out.println("Turn crank first!");
	}
}
