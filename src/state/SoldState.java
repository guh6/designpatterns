package state;

/*
 * SoldState - only dispense state needs to be updated with a valid transaction.
 */
public class SoldState implements State{
	GumballMachine machine;
	
	public SoldState(GumballMachine machine) {
		this.machine = machine;
	}

	public void insertQuarter() {
		System.out.println("Please wait, we're already giving you a gumball");		
	}

	public void ejectQuarter() {
		System.out.println("Sorry, quarter has been eaten");		
	}

	public void turnCrank() {
		System.out.println("Can't trun the crank twice, buddy");
	}

	public void dispense() {
		machine.releaseBall();
		if (machine.getCount() > 0 ) {
			machine.setState(machine.getNoQuarterState());
		} else {
			System.out.println("Sold out!");
			machine.setState(machine.getSoldOutState());
		}
	}
}
