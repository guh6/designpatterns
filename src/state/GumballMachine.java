package state;

public class GumballMachine {
	
	State soldOutState;
	State noQuarterState;
	State soldState;
	State hasQuarterState;
	
	// Set the default machine values
	State state = soldOutState; 
	int count = 0;
	
	public GumballMachine(int numberOfGumballs) {
		this.count = numberOfGumballs;
		this.soldOutState = new SoldOutState(this);
		this.noQuarterState = new NoQuarterState(this);
		this.soldState = new SoldState(this);
		this.hasQuarterState = new HasQuarterState(this);		
		
		if(numberOfGumballs > 0 ) {
			this.state = noQuarterState;
		}
	}

	public void insertQuarter() {
		this.state.insertQuarter();
	}

	public void ejectQuarter() {
		this.state.ejectQuarter();
	}

	public void turnCrank() {
		this.state.turnCrank(); // HasQuarterState -> SoldState
		this.state.dispense(); // SoldState#dispense
	}

	void releaseBall() {
		System.out.println("Gumball running down...");
		if (count !=0 ) {
			count = count - 1;
		}
	}
		
	public int getCount() {
		return count;
	}
	
	public void refill(int count) {
		this.count = count;
		state = noQuarterState;
	}
	
	public State getSoldOutState() {
		return this.soldOutState;
	}
	
	public State getNoQuarterState() {
		return this.noQuarterState;
	}
	
	public State getHasQuarterState() {
		return this.hasQuarterState;
	}
	
	public State getSoldState() {
		return this.soldState;
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public String toString() {
		return "Gumballs: " + this.count;
	}
	

}
