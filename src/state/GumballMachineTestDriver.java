package state;

public class GumballMachineTestDriver {
	public static void main(String args[]) {
		GumballMachine machine = new GumballMachine(5);
		System.out.println(machine);
		
		machine.insertQuarter();
		machine.turnCrank();
		
		System.out.println(machine);
		machine.insertQuarter();
		machine.turnCrank();
		
		machine.insertQuarter();
		machine.turnCrank();
		
		machine.insertQuarter();
		machine.turnCrank();

		
		machine.insertQuarter();
		machine.turnCrank();
		
		System.out.println(machine);

	}
}
