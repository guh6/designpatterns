package state;

/*
 * SoldOutState - no valid transactions can occur, so
 * we can just print a message.
 */
public class SoldOutState implements State {
	
	GumballMachine machine;
	
	public SoldOutState(GumballMachine machine) {
		this.machine = machine;
	}

	public void insertQuarter() {
		System.out.println("Sorry, no more gumballs");
	}

	public void ejectQuarter() {
		System.out.println("Sorry, no more gumballs");
	}

	public void turnCrank() {
		System.out.println("Sorry, no more gumballs");		
	}

	public void dispense() {
		System.out.println("Sorry, no more gumballs");		
	}

}
