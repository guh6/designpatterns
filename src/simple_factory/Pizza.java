package simple_factory;

abstract public class Pizza {
	String type;
	
	public Pizza(String type){
		this.type = type;
	}
	
	public void prepare(){
		System.out.println("Preparing pizza - " + type);;
	}
	
	public void bake(){
		System.out.println("Baking pizza - " + type);
	}
	
	public void cut(){
		System.out.println("Cutting it - " + type);
	}
}
