package simple_factory;

public class PizzaFactory {

	public Pizza createPizza(String type) throws Exception{
		Pizza p = null;
		
		if(type.equals("g"))
		{
			p = new GreekPizza();
		}
		else if(type.equals("c"))
		{
			p = new CheesePizza();
		}
		else
		{
			throw new Exception("Invalid Pizza :(");
		}
		
		return p;
	}
}
