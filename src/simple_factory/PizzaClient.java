package simple_factory;

public class PizzaClient {
	
	static PizzaFactory pizzaFactory = new PizzaFactory();

	public static void main(String args[]){
		orderPizza("c");
		orderPizza("g");		
	}
	
	public static void orderPizza(String type){
		Pizza p;
		try{
			p = pizzaFactory.createPizza(type);
			p.prepare();
			p.bake();
			p.cut();			
		} catch(Exception e){
			System.out.println("Error making pizza. Did you put hte correct type?");
		}
	}
}
