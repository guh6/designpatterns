package simple_factory;

public class CheesePizza extends Pizza{
	public CheesePizza(){
		super("Cheesy Pizza");
	}
}
