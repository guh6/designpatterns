package iterator;

import java.util.ArrayList;

public class PancakeHouseMenu implements Menu {

	ArrayList<String> menu = new ArrayList();
	
	public PancakeHouseMenu(){
		this.menu.add("Pancakes");
		this.menu.add("Tea");
	}
	
	public ArrayList<String> getMenu(){
		return this.menu;
	}
	
	@Override
	public Iterator createIterator() {
		return new PancakeHouseMenuIterator(menu);
	}

}
