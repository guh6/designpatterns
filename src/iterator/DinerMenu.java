package iterator;

public class DinerMenu implements Menu{
	static final int MAX_ITEMS = 6; 
	int numberOfItems = 0;
	String[] menuItems;
	
	public DinerMenu(){
		menuItems = new String[MAX_ITEMS];
		menuItems[0] = "BLT";
		menuItems[1] = "Hamburger";
		menuItems[2] = "Carrots";
		menuItems[3] = "Tofu";
		menuItems[4] = "Milk";
		menuItems[5] = "Pancakes";
		this.numberOfItems = 6;
	}
	
	public String[] getMenuItems(){
		return menuItems;
	}
	
	@Override
	public iterator.Iterator createIterator() {
		return new DinerMenuIterator(menuItems);
	}
	
}
