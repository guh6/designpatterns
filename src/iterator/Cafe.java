package iterator;

public class Cafe {

	public static void main(String args[]){
		PancakeHouseMenu pM = new PancakeHouseMenu();
		DinerMenu dM = new DinerMenu();
		
		Iterator pI = pM.createIterator();
		Iterator dI = dM.createIterator();	
		
		System.out.println("----Diner Menu----");
		while((Boolean)dI.hasNext()){
			System.out.println(dI.next());
		}
		
		System.out.println("----Pancake Menu----");
		while((Boolean)pI.hasNext()){
			System.out.println(pI.next());
		}
	}
	
}
