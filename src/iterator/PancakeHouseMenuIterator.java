package iterator;

import java.util.ArrayList;

public class PancakeHouseMenuIterator implements iterator.Iterator{
	ArrayList<String> pancakeMenu;
	int currentPosition = -1;
	public PancakeHouseMenuIterator(ArrayList<String> menuItems){
		this.pancakeMenu = menuItems;
	}
	
	@Override
	public Object hasNext() {
		if((currentPosition+1) < pancakeMenu.size())
		{
			return true;
		}
		return false;
	}

	@Override
	public Object next() {
		if(currentPosition == 0 )
			return pancakeMenu.get(++currentPosition);
		else if(currentPosition < pancakeMenu.size())
			return pancakeMenu.get(++currentPosition);
		else 
			return "";
	}

}
