package iterator;

public interface Iterator {
	public Object hasNext();
	public Object next();
}
