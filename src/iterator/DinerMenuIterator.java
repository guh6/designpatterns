package iterator;

public class DinerMenuIterator implements iterator.Iterator {
	
	String[] menuItems;
	int position = -1;
	
	public DinerMenuIterator(String[] menuItems){
		this.menuItems = menuItems;
	}

	@Override
	public Object hasNext() {
		if((position + 1) < menuItems.length){
			return true;
		}
		return false;	
	}

	@Override
	public Object next() {
		if(position == 0){
			return menuItems[++position];
		} else if (position  < menuItems.length){
			return menuItems[++position];
		}
		return "";		
	}
}
