package iterator;

public interface Menu {
	public iterator.Iterator createIterator();
}
