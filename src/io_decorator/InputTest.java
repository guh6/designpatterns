package io_decorator;

import java.io.FileNotFoundException;
import java.io.IOException;

public class InputTest {
	public static void main(String args[]) throws FileNotFoundException, IOException{
		
		InputStream inputStream = new FileInputStream("test.txt");
		System.out.println(inputStream.read());
		
		InputStream inputStreamLowered = new LowerCaseInputStream(inputStream);
		System.out.println(inputStreamLowered.read());
		
	}
}
