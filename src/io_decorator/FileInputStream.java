package io_decorator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileInputStream extends InputStream {
	
	private String filename;
	
	public FileInputStream(String filename){
		this.filename = filename;
	}

	@Override
	public String read() throws IOException, FileNotFoundException {
		FileReader fileReader = new FileReader(this.filename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line = null;
		StringBuilder result = new StringBuilder("");
		while((line = bufferedReader.readLine()) != null ){
			result.append(line);
		}
		bufferedReader.close();
		return result.toString();
	}

}
