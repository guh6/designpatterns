package io_decorator;

import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class InputStream {
	public abstract String read() throws IOException, FileNotFoundException;
}
