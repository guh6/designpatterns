package io_decorator;

import java.io.FileNotFoundException;
import java.io.IOException;

public class LowerCaseInputStream extends FilterInputStream {

	public LowerCaseInputStream(InputStream in)
	{
		super(in);
	}
	
	@Override
	public String read() throws FileNotFoundException, IOException {
		return this.input.read().toLowerCase();
	}

}
