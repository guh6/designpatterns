package io_decorator;

public abstract class FilterInputStream extends InputStream {
	
	InputStream input;
	
	public FilterInputStream(InputStream input){
		this.input = input;
	}
}
