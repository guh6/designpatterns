# Purpose #
This is a learning project that has examples of design patterns using Java 1.7+

The primary language used is Java.

# Patterns #
**Enterprise Pattern** deals with enterprise software problems involving follow of a message classified by application layer. 

## Strategy Pattern ##
Defines a family of algorithms, encapsulates each one, and makes them interchangeable via interfaces and HAS-A relationship. Strategy lets the algorithm vary independently from clients that use it. This also uses Composition, where multiple algorithms make up a family.

```java
public interface FlyBehavior{
  public void Fly();
}

public class FlyWithWings implements FlyBehavior{
  public void Fly(){
    println("I am flying!");
  }
}

public class FlyWithRockets implements FlyBehavior{
  public void Fly(){
    println("I am flying with rockets!!!");
  }
}

public abstract class Spartan {
  FlyBehavior flyBehavior;
  public Spartan(FlyBehavior flyBehavior){
    this.flyBehavior = flyBehavior;
  }
}

public class MasterDrauckieChief extends Spartan{
  public MasterDrauckieChief(){
    FlyBehavior myFlyBehavior = new FlyWithRockets();
    super(myFlyBehavior);
  }

public class MasterLizzieChief extends Spartan{
    public MasterLizzieChief(){
      FlyBehavior myFlyBehavior = new FlyWithWings();
      super(myFlyBehavior);
    }
}

```

## Observer Pattern ##
The observer pattern has a Subject(Publisher) and Observer(s) that has a one way dependency from the Observer(s) to the Subject. This is often used in Model-View pattern in MVC, where each object takes in a subject through construction and add itself to the subject's list via register or remove. The Subject calls the Observer's update method to let them know that a state has changed.

By using the interface type, we can interchange anytime of Subject and Observer that implements the specific interface dynamically.

All Observers must implement the Observer interface:
```java
public interface Observer {
	// Mandatory method to deal with an update of a Publisher/Subject
	public void update(float temp, float humidity, float pressure);
}
```

All Subjects must implement the Subject interface:

```java
public interface Subject {
	// Mandatory methods when creating a Publisher/Subject.
	// -> Contains register, remove and notify observer(s).
	public void registerObserver(Observer obs);
	public void removeObserver(Observer obs);
	public void notifyObservers();
}
```

### Java EE ###
The Subject creates the change events and the Observers get notification of those events. The goal is for the subject not know anything about the observers so it can be loosely coupled.  

Example: Create a Rest Endpoint that fires off 3 different services based on an incoming event. Those 3 services are decoupled.

Subject - Customer
```java
@Stateless
@Path("/customers")
public class CustomerEndpoint {

    // This is the Subject that the Observers look for
    @Inject
    private Event<Customer> customerAddEvent;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void newCustomer(Customer customer) {
        customerAddEvent.fire(customer);
    }
}
```
Observers (3)
```java
import javax.enterprise.event.Observes;

public class AuthenticationService {
    // The magic is the @Observes annotation where it expects to receive a customer object for this method
    // Which correlates with CustomerEndpoint.createCustomer(Customer customer)
    public void createAuthenticationCredentials(@Observes Customer customer) {
        // Add credentials
    }
}

public class CustomerService {
    public void createCustomer(@Observes Customer customer) {
        // Add new customer
    }
}

public class EmailService {
    public void sendWelcomeEmail(@Observes Customer customer) {
        // send welcome email
    }
}
```
  - Order is not guaranteed.
  - Observer exception breaks chain
  - Alleviated with @Priority setting
  - Lowest value first

But how do you disambiguate between a Add and Delete event?

  1. Create Qualifier to pick which Class to run with
```java
import javax.inject.Qualifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)                  // I want to run this during runtime
@Target({ElementType.FIELD, ElementType.PARAMETER})  // I want to use this around field and parameter
public @interface CustomerEvent {
    Type value();
    
    enum Type{ ADD, REMOVE }
}
```
  2. Now, use this Qualifier by creating a separate Event queue.
```java
@Stateless
@Path("/customers")
public class CustomerEndpoint {

    // This is the Subject that the Observers look for
    @Inject @CustomerEvent(CustomerEvent.Type.ADD)
    private Event<Customer> customerAddEvent;
    
    @Inject @CustomerEvent(CustomerEvent.Type.REMOVE) 
    Event<Customer> customerRemoveEvent;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void newCustomer(Customer customer) {
        customerAddEvent.fire(customer);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeCustomer(Customer customer) {
        customerRemoveEvent.fire(customer);
    }
}
```
  3. Modify the Observer
```java
public class CustomerService {
    public void createCustomer(@Observes @Priority(10) @CustomerEvent(CustomerEvent.Type.ADD) Customer customer) {
        // Add new customer
    }
    
    public void removeCustomer(@Observes @CustomerEvent(CustomerEvent.Type.REMOVE) Customer customer) {
        // delete customer
    }
}
```
**By default, the Observer's qualifier is `default`, so those events will not be fired if qualifier is not specified.**

To make it async (Sync is the default), In the Observer, change to `@ObserveAsync` and remove `@Prirority`. In the Subject change to `#fireAsync`. You can also configure a custom Executor Threadpool.
```java
@Stateless
@Path("/customers")
public class CustomerEndpoint {

    // This is the Subject that the Observers look for
    @Inject @CustomerEvent(CustomerEvent.Type.ADD)
    private Event<Customer> customerAddEvent;
    
    @Inject @CustomerEvent(CustomerEvent.Type.REMOVE) 
    Event<Customer> customerRemoveEvent;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void newCustomer(Customer customer) {
        // In a scenario where there are 10 or less observers, they all get executed asynchronously
        // Returns an instance of completionStage (exception -> completionException)
        customerAddEvent.fireAsync(customer, NotificationOptions.ofExecutor(new ForkJoinPool(10)));
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeCustomer(Customer customer) {
        CompletionStage<Customer> stage = customerRemoveEvent.fireAsync(customer);
        stage.handle((Customer event, Throwable ex) -> {
            for (Throwable t : ex.getSuppressed()) { // Brings back suppressed excpetions
                // log exceptions
            }
            return event;
        });
    }
}
```  

## Decorator Pattern ##
(Defn): The Decorator Pattern attaches additional responsibilities to an object dynamically. Decorators provide a flexible alternative to sub-classing for extending functionality.  

For a given base Object (e.g., Beverage), there are two abstract classes with the top level being the one that is extended by the concrete sub-class(e.g., Expresso ) and the decorator abstract class (e.g., Expresso w/ Ice). The key idea is that the decorator is also a Beverage type so we're able to wrap multiple condiments for a beverage and the constructor has to set a Beverage type.

Beverage Hierachy:
  - Beverage (abstract)
  - Beverage sub-class (concrete)
  - BeverageDecorator (abstract)
  - BeverageDecorator sub-class(concrete)

Example:
```java
public abstract class Beverage {
	// Won't ever see this description.
	// Use Protected so that the subclasses can see description but others can't.
	protected String description = "Unknown Beverage";

	// Each concrete will inherit this method
	public String getDescription(){
		return description;
	}

	// Abstract so each beverage will need to figure out it's own cost
	public abstract float cost();
}

public abstract class CondimentDecorator extends Beverage {
	// Require the condiment to have its own description
	public abstract String getDescription();
}

public class HouseBlend extends Beverage {
	public HouseBlend(){
		this.description = "House Blend Coffee";
	}

	// House Blend's custom cost
	public float cost() {
		return 0.50f;
	}
}

public class Milk extends CondimentDecorator{
	private Beverage beverage;

	public Milk(Beverage beverage){
		this.beverage = beverage;
	}

	public String getDescription(){
		return this.beverage.getDescription() + ", MILK";
	}

	public float cost(){
		return this.beverage.cost() + 0.10f;
	}
}
```

###Java EE ###
`@Decorator` and `@Delegate`(class to be decorated).

Example: Logger scenario to modify a third-party API that I cannot modify but I can modify how it outputs with existing interface:
```java
public interface LogMessage {
    void printMessage();
    String getMessage();
    void setMessage(String message);
}
```
Implement the same interface `LogMessage`.
```java
@Decorator
public abstract class LogMessageFormatter implements LogMessage {

    @Any
    @Delegate
    @Inject
    private LogMessage logMessage;

    @Override
    public void printMessage() {
        String message = logMessage.getMessage();
        message = LocalDate.now().toString().concat(message);
        logMessage.setMessage(message);
    }
}
```

To have multiple Decorators that relies on the order, add `@Priority` on the Class.
```java
@Priority(20)
@Decorator
public abstract class LogMessageJSONFormatter implements LogMessage {
    
    @Any
    @Delegate
    @Inject
    private LogMessage logMessage;
    
    public void printMessage() {
        String message = logMessage.getMessage();
        String jsonMessage = JsonbBuilder.create().toJson(message);
        logMessage.setMessage(message);
    }
}
```


## Singleton Pattern ##
(Defn): The Singleton Pattern ensures a class has only one instance, and provides a global access point to it.

Singleton Uses:
  - Connection and thread pools
  - Logging
  - Preference and registry objects
  - Device, printer and graphics driver
  - UI Dialog and other modal controls
  - Anywhere you want to ensure a resource only exists only once

Example:
```java
public Singleton {
  private Singleton() {} // Private constructor
  private static Singleton unique; // Keeps track of self, initially null.   

  public static Singleton getInstance() {
    if(unique == null)
    {
      unique = new Singleton();
    }
    return unique;
  }
}
```

Watch for multiple Singletons caused by race conditions. Solutions are:

### 1 - Use eager instantiation ###
```java
private static Singleton uniqueInstance = new Singleton();
```

### 2 - use synchronized keyword but incurs overhead costs of slow method ###
```java
public static synchronized Singleton getInstance(){
  ...
}

```

### 3 - Java EE Edition ###
Simplified version that creates a Singleton Bean using annotations.  
```java
@StartUp
@Singleton
public class SingletonEE {
    
    private Queue<Object> pooledObjects;
    
    @PostContruct
    void constructExpensiveObject(){
        // Do something expensive here like constructor 
    }
    
}

```

For managing concurrent access. EE has two - Container(default) and Bean managed. For Bean managed:
```java
package singleton;

import java.util.Queue;

@ConcurrencyManagement(ConcurrencyManageType.BEAN)
@StartUp
@Singleton
public class SingletonEE {

    private Queue<Object> pooledObjects;

    @PostContruct
    void constructExpensiveObject(){
        // Do something expensive here like constructor
    }
    
    @AccessTimeout(value = 30, unit = TimeUnit.Seconds)
    @Lock(LockType.WRITE)
    public void returnObject(Object obj) {
        pooledObjects.offer(obj);
    }
    
    @Lock(LockType.READ)
    public Object borrowObject() {
        return pooledObjects.poll();
    }

}
```
The Locked is similar to synchronize but more specific. 

## State Pattern ##
The state pattern allows an object to alter its behavior when its internal state changes. The object will appear to change its class.
  -	The pattern encapsulates state into separate classes
  -	The context delegates to the current state to handle requests
  -	Once request is handled, the current state may change - each state has different behavior


## Collection Pattern ##
The iterator pattern provides a way to access the elements of an aggregate object (e.g., list) sequentially without exposing its underlying representation.

Separate what varies and encapsulate them that provides a way to access elements of an aggregate object sequentially without exposing its underlying representation.

We want to be able to go through each object, but without knowing the underlying objects we’re accessing.

The aggregate object provides an iterator that is used by the client that is used to go through the items in the aggregate.


## Factories ##
***Simple factory idiom***: Separate the process of creating concrete objects from the client that uses those objects to reduce the dependency of the client on those concrete implementations.

***Factory method pattern*** defines an interface for creating an object, but lets subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.


## Builder ##
The builder pattern is used to encapsulate the parameters list for a constructor by having a nested static Builder class within the Class.

Imagine an object that has more than five parameters – it would be hard to keep track of the ordering of the parameters and the number of parameters. It would be better to have a Builder that takes care of the parameters of the object by using named setters that is more readable and maintainable.

Steps:
  1.	Create the Class

    a.	Mark the Constructor as private – only the Builder should be able to call it.
  2.	Create an inner static Builder class

    a.	Create a copy of all of the properties from the Outter class

    b.	For all Builder setters, set the return type as the Builder and add return this at the end to allow for chaining.

    c.	Finally, add a build method that returns the outter class Object with the private constructor

Example:
```java
public class Person {
	private final String name;
	private final char gender;
	private final String phoneNumber;
    private final String email;
    /*
     * Private Constructor, notice it's only 4 parameters but imagine it being 10+
     */
	private Person(String name, char gender, String phoneNumber, String email){
		this.name = name;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}
	/*
	 * The Builder static class that will build the Person object
	 */
	public static class PersonBuilder
	{
		private String firstName;
		private String middleName;
		private String lastName;
		private char gender;
		private String phoneNumber;
		private String areaCode;
		private String email;

		// Default settings - Can also use static within static
		private final static String US_CODE = "+1";
		private final static char NO_GENDER = '?';
		private final static String NO_EMAIL = "";
		private final static String NO_MIDDLE = "";		

		// Set the defaults 	 
		{
			this.middleName = NO_MIDDLE;
			this.areaCode = US_CODE;
			this.gender = NO_GENDER;
			this.email = NO_EMAIL;
		}
		/*
		 * Setters - The return type must be the PersonBuilder
		 * itself so that we can chain the other setter methods
		 */
		public PersonBuilder setFirstName(String fName){
			this.firstName = fName ;
			return this;
		}
		public PersonBuilder setMiddleName(String middleName) {
			this.middleName = middleName;
			return this;
		}
		public PersonBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		public PersonBuilder setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}
		public PersonBuilder setAreaCode(String areaCode) {
			this.areaCode = areaCode;
			return this;
		}
		public PersonBuilder setGender(char g){
			this.gender = g;
			return this;
		}		
		/*
		 * Most important method: Build
		 * It builds the object at the end, so it must return the Class type
		 */
		public Person build(){
			String fullName = this.firstName + " " + this.middleName + " " + this.lastName;
			String modifiedPhoneNumber = "(" + this.areaCode + ") " + this.phoneNumber;			
			return new Person(fullName, this.gender, modifiedPhoneNumber, this.email);
		}		
	}
}
```

## Facade ##
  - Provides a unified interface to subsystem.
  - Access to legacy back-end systems
  - Offer coarse-grained access services by only calling one method that calls the rest. 
  - To reduce network calls. E.g., a REST endpoint that makes calls to various subsystems on behalf of the client. 

```java
@Service|@Stateless
public class FacadePattern {
    @Service|@EJB
    private CreditRatingService creditRatingService;
    
    @Service|@JB
    private RepaymentPayabilityService repaymentPayabilityService;
    
    public boolean processLoanApplication(Customer customer, ... ) {
        creditRatingService.checkCreditRating(customer);
        repaymentPayabilityService.doSomething(customer);
        return result; // Result can be whatever the aggregated results from all of the services are. 
    }
}
```

Useful annotations on EJB:
  - Security
  - Interceptors (cross-cutting concerns for aspect programming)
  - Scheduling (cron)
  - Remoting (Use remote EJBs like local EJBs)
  - Asynchronous processing * https://dzone.com/articles/spring-boot-creating-asynchronous-methods-using-as
  - Messaging
  - Web services

# Architectural Software Patterns #
